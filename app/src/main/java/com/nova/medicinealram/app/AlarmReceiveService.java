package com.nova.medicinealram.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Set;

/**
 * Created by kmkyoung on 2014. 6. 9..
 */
public class AlarmReceiveService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        loadAlarm();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("com.nova.medicinealram.setAlarm")) {
                int[] timeLists = intent.getIntArrayExtra("TimeList");
                int id = intent.getIntExtra("MEDICINE_ID", 0);
                String name = intent.getStringExtra("MEDICINE_NAME");
                String ringuri = intent.getStringExtra("MEDICINE_RINGURI");
                boolean alwaysring = intent.getBooleanExtra("MEDICINE_ALWAYS",false);
                addAlarm(id, timeLists, name, ringuri, alwaysring);
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    private void loadAlarm()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("Alram",Context.MODE_PRIVATE);
        Set<String> strings = sharedPreferences.getStringSet("AlramList",null);

        if(strings != null) {
            for (String s : strings) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    int[] timeList = {jsonObject.getInt("morningHour"), jsonObject.getInt("morningMin"), jsonObject.getInt("lunchHour"), jsonObject.getInt("lunchMin"), jsonObject.getInt("eveningHour"), jsonObject.getInt("eveningMin")};
                    addAlarm(jsonObject.getInt("medicineId"), timeList, jsonObject.getString("medicineName"), jsonObject.getString("ringUri"),jsonObject.getBoolean("alwaysRing"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addAlarm(int id, int[] timeLists, String name, String ringuri, boolean alwaysring)
    {

        Calendar curCalendar = Calendar.getInstance();
        AlarmManager alramManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        int year = curCalendar.get(Calendar.YEAR);
        int month = curCalendar.get(Calendar.MONTH);
        int day = curCalendar.get(Calendar.DAY_OF_MONTH);

        if(timeLists[0] != -1)
        {
            Calendar calendar = setCalendar(curCalendar,year,month,day,timeLists[0],timeLists[1]);
            Intent intent = setIntent(id,name,ringuri,timeLists[0],timeLists[1],1,alwaysring);

            PendingIntent pender = PendingIntent.getBroadcast(getApplicationContext(),(id*10)+1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            alramManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pender);
        }
        if(timeLists[2] != -1)
        {
            Calendar calendar = setCalendar(curCalendar,year,month,day,timeLists[2],timeLists[3]);
            Intent intent = setIntent(id,name,ringuri,timeLists[2],timeLists[3],2,alwaysring);

            PendingIntent pender = PendingIntent.getBroadcast(getApplicationContext(),(id*10)+2,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            alramManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pender);
        }
        if(timeLists[4] != -1)
        {
            Calendar calendar = setCalendar(curCalendar,year,month,day,timeLists[4],timeLists[5]);
            Intent intent = setIntent(id,name,ringuri,timeLists[4],timeLists[5],3,alwaysring);

            PendingIntent pender = PendingIntent.getBroadcast(getApplicationContext(),(id*10)+3,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            alramManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pender);
        }

    }

    private Intent setIntent(int id, String name, String ringuri, int hour, int min, int timeflag, boolean alwaysRing)
    {
        Intent intent = new Intent(getApplicationContext(),AlarmReceive.class);
        intent.putExtra("medicineId",id);
        intent.putExtra("medicineName",name);
        intent.putExtra("ringUri",ringuri);
        intent.putExtra("TimeHour",hour);
        intent.putExtra("TimeMin",min);
        intent.putExtra("TimeFlag",timeflag);
        intent.putExtra("alwaysring",alwaysRing);

        return intent;
    }

    private Calendar setCalendar(Calendar curCalendar,int year, int month, int day, int hour, int min)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, min,0);
        calendar.setTimeInMillis(calendar.getTimeInMillis()-calendar.getTimeInMillis()%1000);
        if(curCalendar.getTimeInMillis() > calendar.getTimeInMillis())
            calendar.add(Calendar.DAY_OF_MONTH,1);
        return calendar;
    }
}

package com.nova.medicinealram.app;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

/**
 * Created by kmkyoung on 2014. 6. 9..
 */
public class RebootReceive extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            ComponentName componentName = new ComponentName(context.getPackageName(),AlarmReceiveService.class.getName());
            ComponentName serviceName = context.startService(new Intent().setComponent(componentName));

        }
    }
}
package com.nova.medicinealram.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class AlarmCallActivity extends Activity {
    private TextView medicineName, notiText;
    private Button okButton;
    private Ringtone ringtone;
    private int ringModeBack;
    boolean alwaysRing;
    AudioManager audiomanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_call);
        ringModeBack = 0;
        Intent intent = getIntent();
        alwaysRing = intent.getBooleanExtra("alwaysring",false);
        audiomanager =  (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        medicineName = (TextView)findViewById(R.id.call_activity_text);
        notiText = (TextView)findViewById(R.id.call_activity_text2);
        okButton = (Button)findViewById(R.id.call_activity_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ringtone.stop();
                if(alwaysRing == true) {
                    audiomanager.setRingerMode(ringModeBack);
                }

                finish();
            }
        });
        String time = "";
        switch (intent.getIntExtra("TimeFlag",0))
        {
            case 1:
                time = getResources().getString(R.string.morning);
                break;
            case 2:
                time = getResources().getString(R.string.lunch);
                break;
            case 3:
                time = getResources().getString(R.string.evening);
                break;
        }
        medicineName.setText(time+" "+intent.getStringExtra("medicineName")+getResources().getString(R.string.notitext1));
        notiText.setText(getResources().getString(R.string.notitext2));

        if(alwaysRing == true) {
            ringModeBack = audiomanager.getRingerMode();
            audiomanager.setRingerMode(2);
        }

        Uri currentUri = Uri.parse(intent.getStringExtra("ringUri"));
        ringtone = RingtoneManager.getRingtone(this, currentUri);
        if (ringtone != null) {
            ringtone.play();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.alarm_call, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

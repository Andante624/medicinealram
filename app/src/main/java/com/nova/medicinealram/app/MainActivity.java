package com.nova.medicinealram.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class MainActivity extends Activity {
    private static final int REQUEST_ADD_MEDICINE = 1;
    private static final String defult_font = "SeoulNamsanB.otf";
    public static final int REQUEST_EDIT_MEDICINE = 2;
    private ListView medicineListView;
    private TextView addText;
    private MedicineAdapter medicineAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        if(isMedicineItems())
        {
            loadItems();
        }
        Intent intent = new Intent("com.nova.medicinealram.AlarmReceiveService");
        startService(intent);

    }

    private void init()
    {
        medicineListView = (ListView)findViewById(R.id.medicine_listview);
        LinearLayout addLayout = (LinearLayout)findViewById(R.id.add_medicine);
        addText = (TextView)findViewById(R.id.add_textview);

        medicineAdapter = new MedicineAdapter(this);
        medicineListView.setAdapter(medicineAdapter);

        addLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MedicineAddActivity.class);
                startActivityForResult(intent,REQUEST_ADD_MEDICINE);
            }
        });

        addText.setTypeface(Typeface.createFromAsset(getAssets(), defult_font));
    }

    private Boolean isMedicineItems()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("Alram", Context.MODE_PRIVATE);
        int medicineCount = sharedPreferences.getInt("medicineCount",0);
        if(medicineCount == 0)
            return false;
        else
            return true;
    }

    private void saveItems(MedicineItem newItem)
    {
        SharedPreferences sharedPreferences = getSharedPreferences("Alram", Context.MODE_PRIVATE);
        Set<String> strings = sharedPreferences.getStringSet("AlramList",null);
        if(strings == null)
            strings = new HashSet<String>();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        JSONObject obj = new JSONObject();
        try {
            obj.put("medicineId",newItem.getMedicineId());
            obj.put("medicineName", newItem.getMedicineName());
            obj.put("ringName", newItem.getRingName());
            obj.put("ringUri", newItem.getRingUri());
            obj.put("morningHour",newItem.getMorningHour());
            obj.put("morningMin",newItem.getMorningMin());
            obj.put("lunchHour",newItem.getLunchHour());
            obj.put("lunchMin",newItem.getLunchMin());
            obj.put("eveningHour",newItem.getEveningHour());
            obj.put("eveningMin",newItem.getEveningMin());
            obj.put("alwaysRing",newItem.getAlwaysRing());

            strings.add(obj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        editor.putStringSet("AlramList",strings);
        editor.putInt("medicineCount",medicineAdapter.getCount());
        editor.commit();
    }

    private void loadItems()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("Alram",Context.MODE_PRIVATE);
        Set<String> strings = sharedPreferences.getStringSet("AlramList",null);

        if(strings != null) {
            for (String s : strings) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    MedicineItem medicineItem = new MedicineItem(jsonObject.getInt("medicineId"), jsonObject.getString("medicineName"), jsonObject.getInt("morningHour"), jsonObject.getInt("morningMin"), jsonObject.getInt("lunchHour"), jsonObject.getInt("lunchMin"), jsonObject.getInt("eveningHour"), jsonObject.getInt("eveningMin"), jsonObject.getString("ringName"), jsonObject.getString("ringUri"),jsonObject.getBoolean("alwaysRing"));
                    medicineAdapter.addItem(medicineItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(resultCode == RESULT_OK && (requestCode == REQUEST_ADD_MEDICINE || requestCode == REQUEST_EDIT_MEDICINE))
        {
            int[] timeLists = intent.getIntArrayExtra("TimeList");
            int position = intent.getIntExtra("MEDICINE_POSITION",-1);
            int id = intent.getIntExtra("MEDICINE_ID",0);
            String name = intent.getStringExtra("MEDICINE_NAME");
            String ring = intent.getStringExtra("MEDICINE_RING");
            String ringuri = intent.getStringExtra("MEDICINE_RINGURI");
            boolean alwaysring = intent.getBooleanExtra("MEDICINE_ALWAYS",false);

            if(position != -1)
            {
                MedicineItem editItem = medicineAdapter.getItem(position);
                editItem.setMedicineName(name);
                editItem.setTimeList(timeLists);
                editItem.setRingName(ring);
                editItem.setRingUri(ringuri);
                editItem.setAlwaysRing(alwaysring);
                medicineAdapter.saveAllItems();
            }
            else
            {
                long now = System.currentTimeMillis();
                SimpleDateFormat curdateFormat = new SimpleDateFormat("ddHHmmss");
                String str = curdateFormat.format(new Date(now));
                id = Integer.parseInt(str);

                MedicineItem newItem = new MedicineItem(id, name, timeLists[0], timeLists[1], timeLists[2], timeLists[3], timeLists[4], timeLists[5], ring, ringuri, alwaysring);
                medicineAdapter.addItem(newItem);
                saveItems(newItem);
            }

            sendAlarmService(id,timeLists,name,ringuri,alwaysring);
            medicineAdapter.notifyDataSetChanged();
        }
    }

    public void sendAlarmService(int id, int[] timeLists, String name, String ringuri, boolean alwaysring)
    {
        Intent addAlarmIntent = new Intent(this,AlarmReceiveService.class);
        addAlarmIntent.putExtra("TimeList",timeLists);
        addAlarmIntent.putExtra("MEDICINE_NAME",name);
        addAlarmIntent.putExtra("MEDICINE_RINGURI",ringuri);
        addAlarmIntent.putExtra("MEDICINE_ID",id);
        addAlarmIntent.putExtra("MEDICINE_ALWAYS",alwaysring);
        addAlarmIntent.setAction("com.nova.medicinealram.setAlarm");
        startService(addAlarmIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

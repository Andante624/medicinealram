package com.nova.medicinealram.app;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kmkyoung on 2014. 6. 3..
 */
public class MedicineAdapter extends BaseAdapter {
    private static final String defult_font = "SeoulNamsan.ttf";
    private static final String pills_color[] = {"pills_blue","pills_yellow","pills_green","pills_orange","pills_red","pills_purple","pills_blue2","pills_green2"};
    private Context context;
    private ArrayList<MedicineItem> medicineItemList = new ArrayList<MedicineItem>();

    MedicineAdapter(Context context)
    {
        this.context = context;
    }

    public void addItem(MedicineItem newItem)
    {
        medicineItemList.add(newItem);
    }

    @Override
    public int getCount() {
        return medicineItemList.size();
    }

    @Override
    public MedicineItem getItem(int position) {
        return medicineItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return medicineItemList.get(position).getMedicineId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context,R.layout.medicine_item,null);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int timeList[] = makeTimeList(position);
                sendMedicineData(timeList,position);
            }
        });
        ImageView medicine_color_icon = (ImageView)view.findViewById(R.id.pills_color_icon);
        TextView medicine_name = (TextView)view.findViewById(R.id.medicine_name);
        TextView delete_text = (TextView)view.findViewById(R.id.medicine_delete_text);
        medicine_name.setTypeface(Typeface.createFromAsset(context.getAssets(), defult_font));
        delete_text.setTypeface(Typeface.createFromAsset(context.getAssets(),defult_font));
        ImageView medicine_delete = (ImageView)view.findViewById(R.id.medicine_deleteButton);
        ImageView morningIcon = (ImageView)view.findViewById(R.id.medicine_morning_icon);
        ImageView lunchIcon = (ImageView)view.findViewById(R.id.medicine_lunch_icon);
        ImageView eveningIcon = (ImageView)view.findViewById(R.id.medicine_evening_icon);
        if(medicineItemList.get(position).isMorningTime())
            morningIcon.setImageResource(R.drawable.morning_color);
        if(medicineItemList.get(position).isLunchTime())
            lunchIcon.setImageResource(R.drawable.afternoon_on);
        if(medicineItemList.get(position).isEveningTime())
            eveningIcon.setImageResource(R.drawable.night_color);
        medicine_color_icon.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+pills_color[position%8],null,context.getPackageName())));
        medicine_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlarm(position);
                medicineItemList.remove(position);
                saveAllItems();
                notifyDataSetChanged();
            }
        });
        medicine_name.setText(medicineItemList.get(position).getMedicineName());
        return view;
    }

    private int[] makeTimeList(int position)
    {
        int timeList[] = new int[6];
        timeList[0] = medicineItemList.get(position).getMorningHour();
        timeList[1] = medicineItemList.get(position).getMorningMin();
        timeList[2] = medicineItemList.get(position).getLunchHour();
        timeList[3] = medicineItemList.get(position).getLunchMin();
        timeList[4] = medicineItemList.get(position).getEveningHour();
        timeList[5] = medicineItemList.get(position).getEveningMin();
        return timeList;
    }

    private void sendMedicineData(int[] timeList, int position)
    {
        Intent intent = new Intent(context, MedicineAddActivity.class);
        intent.setAction("com.nova.medicinealram.editMedicine");
        intent.putExtra("TimeList",timeList);
        intent.putExtra("MEDICINE_ID",medicineItemList.get(position).getMedicineId());
        intent.putExtra("MEDICINE_NAME", medicineItemList.get(position).getMedicineName());
        intent.putExtra("MEDICINE_RING", medicineItemList.get(position).getRingName());
        intent.putExtra("MEDICINE_RINGURI", medicineItemList.get(position).getRingUri());
        intent.putExtra("MEDICINE_POSITION", position);
        intent.putExtra("MEDICINE_ALWAYS", medicineItemList.get(position).getAlwaysRing());
        ((Activity)context).startActivityForResult(intent, MainActivity.REQUEST_EDIT_MEDICINE);
    }

    private void deleteAlarm(int position)
    {
        Intent intent = new Intent(context,AlarmReceive.class);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if(medicineItemList.get(position).isMorningTime())
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,(10*medicineItemList.get(position).getMedicineId())+1,intent,0);
            pendingIntent.cancel();
            alarmManager.cancel(pendingIntent);
        }
        if(medicineItemList.get(position).isLunchTime())
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,(10*medicineItemList.get(position).getMedicineId())+2,intent,0);
            pendingIntent.cancel();
            alarmManager.cancel(pendingIntent);
        }
        if(medicineItemList.get(position).isEveningTime())
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,(10*medicineItemList.get(position).getMedicineId())+3,intent,0);
            pendingIntent.cancel();
            alarmManager.cancel(pendingIntent);
        }

    }

    public void saveAllItems()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Alram", Context.MODE_PRIVATE);
        Set<String> strings = new HashSet<String>();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        for(int i=0; i<medicineItemList.size(); i++) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("medicineId", medicineItemList.get(i).getMedicineId());
                obj.put("medicineName", medicineItemList.get(i).getMedicineName());
                obj.put("ringName", medicineItemList.get(i).getRingName());
                obj.put("ringUri", medicineItemList.get(i).getRingUri());
                obj.put("morningHour", medicineItemList.get(i).getMorningHour());
                obj.put("morningMin", medicineItemList.get(i).getMorningMin());
                obj.put("lunchHour", medicineItemList.get(i).getLunchHour());
                obj.put("lunchMin", medicineItemList.get(i).getLunchMin());
                obj.put("eveningHour", medicineItemList.get(i).getEveningHour());
                obj.put("eveningMin", medicineItemList.get(i).getEveningMin());
                obj.put("alwaysRing", medicineItemList.get(i).getAlwaysRing());

                strings.add(obj.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        editor.putStringSet("AlramList", strings);
        editor.putInt("medicineCount",medicineItemList.size());
        editor.commit();
    }
}
package com.nova.medicinealram.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

/**
 * Created by kmkyoung on 2014. 6. 10..
 */
public class AlarmReceive extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra("medicineId",0);
        String name = intent.getStringExtra("medicineName");
        String ringuri = intent.getStringExtra("ringUri");
        int hour = intent.getIntExtra("TimeHour",0);
        int min = intent.getIntExtra("TimeMin",0);
        int timeflag = intent.getIntExtra("TimeFlag", 0);
        boolean alwaysring = intent.getBooleanExtra("alwaysring",false);

        Intent callActivity = new Intent(context,AlarmCallActivity.class);
        callActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        callActivity.putExtra("ringUri",ringuri);
        callActivity.putExtra("medicineName",name);
        callActivity.putExtra("TimeFlag",timeflag);
        callActivity.putExtra("alwaysring",alwaysring);
        context.startActivity(callActivity);

        //다음날 동일한 이벤트가 발생할 수 있도록 새로운 intent 등록
        setNextAlarm(context,hour,min,name,ringuri,timeflag,id, alwaysring);

    }

    private void setNextAlarm(Context context,int hour, int min, String name, String ringuri, int timeflag, int id, boolean alwaysring)
    {
        Calendar calendar = Calendar.getInstance();
        AlarmManager alramManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Calendar calendarSub = Calendar.getInstance();
        calendarSub.set(year, month, day, hour, min, 0);
        calendarSub.setTimeInMillis(calendarSub.getTimeInMillis()-calendarSub.getTimeInMillis()%1000);
        calendarSub.add(Calendar.DAY_OF_MONTH,1);

        Intent intent = new Intent(context,AlarmReceive.class);
        intent.putExtra("ringUri",ringuri);
        intent.putExtra("TimeHour",hour);
        intent.putExtra("TimeMin",min);
        intent.putExtra("medicineId",id);
        intent.putExtra("medicineName",name);
        intent.putExtra("TimeFlag",timeflag);
        intent.putExtra("alwaysring",alwaysring);

        PendingIntent pender = PendingIntent.getBroadcast(context,(id*10)+1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        alramManager.set(AlarmManager.RTC_WAKEUP, calendarSub.getTimeInMillis(),pender);
    }
}
package com.nova.medicinealram.app;

/**
 * Created by kmkyoung on 2014. 6. 3..
 */
public class MedicineItem {
    private int medicineId;
    private String medicineName;
    private int morningHour, lunchHour, eveningHour;
    private int morningMin, lunchMin, eveningMin;
    private String ringName, ringUri;
    private boolean alwaysRing;

    MedicineItem(int medicineid, String medicinename, int morninghour, int morningmin, int lunchhour, int lunchmin, int eveninghour, int eveningmin, String ringname, String ringuri, Boolean always)
    {
        medicineId = medicineid;
        medicineName = medicinename;
        morningHour = morninghour;
        morningMin = morningmin;
        lunchHour = lunchhour;
        lunchMin = lunchmin;
        eveningHour = eveninghour;
        eveningMin = eveningmin;
        ringName = ringname;
        ringUri = ringuri;
        alwaysRing = always;
    }

    public int getMedicineId() { return medicineId; }
    public String getMedicineName() { return medicineName; }
    public int getMorningHour() { return morningHour; }
    public int getMorningMin() { return morningMin; }
    public int getLunchHour() { return lunchHour; }
    public int getLunchMin() { return lunchMin; }
    public int getEveningHour() { return eveningHour; }
    public int getEveningMin() { return eveningMin; }
    public boolean getAlwaysRing() { return alwaysRing; }
    public String getRingName() { return ringName; }
    public String getRingUri() { return ringUri; }
    public Boolean isMorningTime() { return (morningHour != -1)? true:false;}
    public Boolean isLunchTime() { return (lunchHour != -1)? true:false;}
    public Boolean isEveningTime() { return (eveningHour != -1)? true:false;}

    public void setMedicineName(String name) {medicineName = name;}
    public void setRingName(String ringname) {ringName = ringname;}
    public void setRingUri(String ringuri) {ringUri = ringuri;}
    public void setAlwaysRing(boolean always) {alwaysRing = always;}
    public void setTimeList(int[] TimeList)
    {
        morningHour = TimeList[0];
        morningMin = TimeList[1];
        lunchHour = TimeList[2];
        lunchMin = TimeList[3];
        eveningHour = TimeList[4];
        eveningMin = TimeList[5];
    }

}

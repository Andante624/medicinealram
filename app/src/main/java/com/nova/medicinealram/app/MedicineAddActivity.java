package com.nova.medicinealram.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;


public class MedicineAddActivity extends Activity implements View.OnClickListener{
    private static final int REQUEST_RINGTONE = 3;
    private final int MORNING = 1;
    private final int LUNCH = 2;
    private final int EVENING = 3;
    private String ringURI = "";
    private int MedicineId = 0;
    private EditText medicineNameText;
    private TextView morningText, lunchText, eveningText, ringText;
    private Button morningButton, lunchButton, eveningButton, okButton, cancelButton, nameChoiceButton, ringButton;
    private CheckBox ringCheckalways;
    private String ringName = "";
    private int position = -1;
    private int[] timeList = new int[]{-1,-1,-1,-1,-1,-1};
    private int choiceTime = 1;
    private int state = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_add);
        init();

        Intent intent = getIntent();
        if (intent.getAction() == "com.nova.medicinealram.editMedicine")
        {
            timeList = intent.getIntArrayExtra("TimeList");
            medicineNameText.setText(intent.getStringExtra("MEDICINE_NAME"));
            MedicineId = intent.getIntExtra("MEDICINE_ID",0);
            ringName = intent.getStringExtra("MEDICINE_RING");
            ringURI = intent.getStringExtra("MEDICINE_RINGURI");
            position = intent.getIntExtra("MEDICINE_POSITION",-1);
            ringCheckalways.setChecked(intent.getBooleanExtra("MEDICINE_ALWAYS",false));
            updateView();
        }
        else
        {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
            ringURI = uri.toString();
            try {
                ringName = ringtone.getTitle(getApplicationContext());
                ringText.setText(ringName);
            }catch(NullPointerException e)
            {
                Log.d("kmkyoung","Null Pointer Exception");
            }
        }

        setListener();
    }

    private void init()
    {
        medicineNameText = (EditText) findViewById(R.id.add_medicine_name_edittext);
        morningText = (TextView) findViewById(R.id.add_medicine_morning_dateview);
        lunchText = (TextView) findViewById(R.id.add_medicine_lunch_dateview);
        eveningText = (TextView) findViewById(R.id.add_medicine_evening_dateview);
        morningButton = (Button) findViewById(R.id.add_medicine_morning_button);
        lunchButton = (Button) findViewById(R.id.add_medicine_lunch_button);
        eveningButton = (Button) findViewById(R.id.add_medicine_evening_button);
        ringText = (TextView) findViewById(R.id.add_medicine_ring_text);
        ringText.setSelected(true);
        ringCheckalways = (CheckBox)findViewById(R.id.ring_check_always);
        okButton = (Button) findViewById(R.id.add_medicine_ok_button);
        cancelButton = (Button) findViewById(R.id.add_medicine_cancel_button);
        nameChoiceButton = (Button)findViewById(R.id.add_medicine_name_choicebutton);
        ringButton = (Button)findViewById(R.id.add_medicine_ring_button);
    }

    private void updateView()
    {
        if(timeList[0] != -1)
        {
            morningText.setText(getAMPM(timeList[0],timeList[1]));
            choiceTime = (choiceTime ==0)? 2 : choiceTime*2;
            morningText.setTextSize(17);
            morningButton.setBackgroundResource(R.drawable.positive_button_selector);
        }
        if(timeList[2] != -1)
        {
            lunchText.setText(getAMPM(timeList[2], timeList[3]));
            choiceTime = (choiceTime ==0)? 3 : choiceTime*3;
            lunchText.setTextSize(17);
            lunchButton.setBackgroundResource(R.drawable.positive_button_selector);
        }
        if(timeList[4] != -1)
        {
            eveningText.setText(getAMPM(timeList[4],timeList[5]));
            choiceTime = (choiceTime ==0)? 5 : choiceTime*5;
            eveningText.setTextSize(17);
            eveningButton.setBackgroundResource(R.drawable.positive_button_selector);
        }
        ringText.setText(ringName);
    }

    private void setListener()
    {
        morningText.setOnClickListener(this);
        lunchText.setOnClickListener(this);
        eveningText.setOnClickListener(this);
        morningButton.setOnClickListener(this);
        lunchButton.setOnClickListener(this);
        eveningButton.setOnClickListener(this);
        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        nameChoiceButton.setOnClickListener(this);
        ringButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.add_medicine_morning_dateview:
                state = MORNING;
                break;
            case R.id.add_medicine_lunch_dateview:
                state = LUNCH;
                break;
            case R.id.add_medicine_evening_dateview:
                state = EVENING;
                break;
            case R.id.add_medicine_ring_button:
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE,getResources().getString(R.string.choicer_ring));
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT,false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT,false);
                startActivityForResult(intent,REQUEST_RINGTONE);
                break;
            case R.id.add_medicine_ok_button:
                sendData();
                break;
            case R.id.add_medicine_cancel_button:
                finish();
                break;
            case R.id.add_medicine_morning_button:
                deleteData(MORNING);
                break;
            case R.id.add_medicine_lunch_button:
                deleteData(LUNCH);
                break;
            case R.id.add_medicine_evening_button:
                deleteData(EVENING);
                break;
            case R.id.add_medicine_name_choicebutton:
                final String items[] = { getResources().getString(R.string.defualt_1), getResources().getString(R.string.defualt_2), getResources().getString(R.string.defualt_3),getResources().getString(R.string.defualt_4), getResources().getString(R.string.defualt_5), getResources().getString(R.string.defualt_6)};
                AlertDialog.Builder ab = new AlertDialog.Builder(this);
                ab.setSingleChoiceItems(items, 0,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                medicineNameText.setText(items[whichButton]);
                                dialog.dismiss();
                            }
                        }
                );
                ab.show();
                break;

        }
        if(state > 0)
        {
            Calendar calendar = Calendar.getInstance();

            if(choiceTime != 1)
            {
                if(state == MORNING && choiceTime %2 == 0)
                    calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),timeList[0],timeList[1]);
                else if(state == LUNCH && choiceTime %3 == 0)
                    calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),timeList[2],timeList[3]);
                else if(state == LUNCH && choiceTime %5 == 0)
                    calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH),timeList[4],timeList[5]);
            }
            TimePickerDialog dialogTime = new TimePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, myTimeSetListener, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), false);
            dialogTime.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    state= 0 ;
                }
            });
            dialogTime.show();
        }
    }

    private void deleteData(int state) {
        switch (state) {
            case MORNING:
                timeList[0] = timeList[1] = -1;
                choiceTime /= 2;
                morningText.setText("");
                morningText.setTextSize(13);
                morningButton.setBackgroundResource(R.drawable.negative_button_selector);
                break;
            case LUNCH:
                timeList[2] = timeList[3] = -1;
                choiceTime /= 3;
                lunchText.setText("");
                lunchText.setTextSize(13);
                lunchButton.setBackgroundResource(R.drawable.negative_button_selector);
                break;
            case EVENING:
                timeList[4] = timeList[5] = -1;
                choiceTime /= 5;
                eveningText.setText("");
                eveningText.setTextSize(13);
                eveningButton.setBackgroundResource(R.drawable.negative_button_selector);
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(resultCode == RESULT_OK && requestCode == REQUEST_RINGTONE)
        {
           Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
           if(uri != null)
           {
               ringURI = uri.toString();
               Ringtone ringtone = RingtoneManager.getRingtone(this,uri);
               ringName = ringtone.getTitle(this);
               ringText.setText(ringName);
           }
        }
    }

    private void sendData() {
        if (!medicineNameText.getText().toString().equals("") && choiceTime != 1)
        {
            Intent intent = new Intent();
            intent.putExtra("TimeList", timeList);
            intent.putExtra("MEDICINE_NAME", medicineNameText.getText().toString());
            intent.putExtra("MEDICINE_RING", ringName);
            intent.putExtra("MEDICINE_RINGURI", ringURI);
            intent.putExtra("MEDICINE_ID",MedicineId);
            intent.putExtra("MEDICINE_POSITION",position);
            intent.putExtra("MEDICINE_ALWAYS",ringCheckalways.isChecked());
            setResult(RESULT_OK, intent);
            finish();
        }
        else
        {
            AlertDialog.Builder ab=new AlertDialog.Builder(this);
            ab.setMessage(getResources().getString(R.string.notiaddtext));
            ab.setPositiveButton(getResources().getString(R.string.ok), null);
            ab.show();
        }
    }

    private String getAMPM(int hour, int min)
    {
        String timeText ="";
        if(hour>=12)
            timeText = getResources().getString(R.string.pm)+((hour == 12)?hour:hour-12)+":"+((min <10)?"0"+min:min);
        else
            timeText = getResources().getString(R.string.am)+((hour == 0)?12:hour)+":"+((min <10)?"0"+min:min);
        return timeText;
    }

    private TimePickerDialog.OnTimeSetListener myTimeSetListener
            = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar.getInstance();
            switch(state)
            {
                case MORNING:
                    timeList[0] = hourOfDay;
                    timeList[1] = minute;
                    morningText.setText(getAMPM(hourOfDay, minute));
                    morningText.setTextSize(17);
                    choiceTime = (choiceTime ==0)? 2 : choiceTime*2;
                    morningButton.setBackgroundResource(R.drawable.positive_button_selector);
                    break;
                case LUNCH:
                    timeList[2] = hourOfDay;
                    timeList[3] = minute;
                    lunchText.setText(getAMPM(hourOfDay,minute));
                    lunchText.setTextSize(17);
                    choiceTime = (choiceTime ==0)? 3 : choiceTime*3;
                    lunchButton.setBackgroundResource(R.drawable.positive_button_selector);
                    break;
                case EVENING:
                    timeList[4] = hourOfDay;
                    timeList[5] = minute;
                    eveningText.setText(getAMPM(hourOfDay,minute));
                    eveningText.setTextSize(17);
                    choiceTime = (choiceTime ==0)? 5 : choiceTime*5;
                    eveningButton.setBackgroundResource(R.drawable.positive_button_selector);
                    break;
            }
        }
    };




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.medicine_add, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
